"""suorganizer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from onboarding.views import Index
from onboarding.views import OptionsView, help, language
from onboarding.views import accesscode
from onboarding.views import incident
from onboarding.stopit_getcountries import get_countries
from onboarding.stopit_getcountries import get_states
from onboarding.stopit_getcountries import get_orgs, get_org_logininfo
from onboarding.stopit_login import login, logout, messenger_via_login, get_toc, check_toc_acceptance
from onboarding.stopit_login import check_tos_for_logininfo
from onboarding.stopit_incident import fileincident
from onboarding.stopit_contacts import show_contacts, show_contact, save_contact


admin.autodiscover()

urlpatterns = patterns('',
        url(r'^$', Index.as_view()),
        url(r'^admin/', include(admin.site.urls)),
        url(r'^options/', OptionsView.as_view()),
        url(r'^accesscode/', accesscode.as_view()),
        url(r'^findcountry/', get_countries),
        url(r'^findorg/', get_orgs),
        url(r'^findstate/', get_states),
        url(r'^enterlogininfo/', get_org_logininfo),
        url(r'^login/', check_toc_acceptance),
        url(r'^accept_tos/', login),
        url(r'^loginviainfo/', check_tos_for_logininfo),
        url(r'^logout/', logout),
        url(r'^incident/', incident.as_view()),
        url(r'^file_incident/', fileincident),
        url(r'^messenger/', messenger_via_login),
        url(r'^manage_contacts/', show_contacts),
        url(r'^edit_contact/', show_contact),
        url(r'^save_contact/', save_contact),
        url(r'^language/', language.as_view()),
        url(r'^help/', help.as_view()),
        url(r'^terms/', get_toc),
)