from django.views.generic import View
from django.conf import settings
from django.shortcuts import render
from base64 import b64decode, b64encode
import requests, json
import logging
import HTMLParser


logging.basicConfig()
# Get an instance of a logger
logger = logging.getLogger('project.stopit.logger')


def get_countries(request, error_message=None):
        params = {}
        logger.error('Get countries got called')
        if error_message is not None:
            params["error_message"] = error_message
            logger.error(error_message)
        try:
            try:
                org_type = request.GET['org_type']
            except:
                org_type = request.POST.get('org_type', 1)

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_response = requests.get(settings.API_ENDPOINT_URL + '/v1/countries', headers=json_headers)
            json_object = json_response.json()
            params["name"] = "success"
            params["country_name"] = json_object['data']['countries']
            params["org_type"] = org_type

            params["country_state_list"] = json.dumps(json_object['data'])

        except:
            params["name"] = "Django"
        return render(request, 'findcountry.html', params)


def get_states(request):
        params = {}
        logger.error('Get states got called')
        try:
            try:
                org_type = request.GET['org_type']
                param_country = request.GET['code']
            except:
                org_type = request.POST['org_type']
                param_country = request.POST['code']

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_response = requests.get(settings.API_ENDPOINT_URL+ '/v1/countries', headers=json_headers)
            json_object = json_response.json()
            params["name"] = "success"
            all_countries = json_object['data']['countries']
            params["org_type"] = org_type
            for country in all_countries:
                if country['code'] == param_country:
                    params["states"] = country["states"]
                    params["country_code"] = param_country
                    return render(request, 'findstate.html', params)
            return get_states(request)
        except:
            params["name"] = "Django"
        return get_countries(request)


def get_orgs(request):
        params = {}
        logger.error('Get orgs got called')
        try:
            try:
                org_type = request.GET['org_type']
                country_code = request.GET['country_code']
                state_code = request.GET['state_code']
            except:
                org_type = request.POST['org_type']
                country_code = request.POST['country_code']
                state_code = request.POST['state_code']

            json_data = {'country_id': country_code,
                         'state_id': state_code,
                         'org_name': "",
                         'org_type_id': org_type}

            if org_type == "1":
                params['IMAGE_FILE'] = 'school.png'
                params['ORG_TYPE_DESC'] = 'School'
            elif org_type == "2":
                params['IMAGE_FILE'] = 'college.png'
                params['ORG_TYPE_DESC'] = 'College'
            else:
                params['IMAGE_FILE'] = 'work.png'
                params['ORG_TYPE_DESC'] = 'Organization'

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            json_response = requests.post(settings.API_ENDPOINT_URL+ '/v1/orgs/search',
                                          data=json.dumps(json_data),
                                          headers=json_headers)

            json_object = json_response.json()
            params["orgs"] = json_object['data']['orgs']
            params["org_type"] = org_type
            params["country_code"] = country_code
            params["state_code"] = state_code
            return render(request, 'findorg.html', params)
        except Exception as e:
            params["org_type"] = org_type
            params["code"] = country_code

            error_message = "No organizations were found."

            if str(org_type) == str(1):
                error_message = "No schools were found."
            elif str(org_type) == str(2):
                error_message = "No colleges were found."

            params["error_message"] = error_message;
            logger.error(e)
        return get_countries(request, error_message)


def get_org_logininfo(request, org_type=None, country_code=None, state_code=None, org_name=None, error_message=None):
        params = {}
        logger.error('Get orgs got called')
        try:
            if org_name is None:
                html_parser = HTMLParser.HTMLParser()
                try:
                    org_type = request.GET['org_type']
                    country_code = request.GET['country_code']
                    state_code = request.GET['state_code']
                    org_name_orig = html_parser.unescape(request.GET['org_name'])
                    org_name = org_name_orig.replace('%20', ' ')
                except:
                    org_type = request.POST['org_type']
                    country_code = request.POST['country_code']
                    state_code = request.POST['state_code']
                    org_name = request.POST['org_name']
            else:
                if error_message is not None:
                    params["error_message"] = error_message

            json_data = {'country_id': country_code,
                         'state_id': state_code,
                         'org_name': org_name,
                         'org_type_id': org_type}

            logger.error(json.dumps(json_data))

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            json_response = requests.post(settings.API_ENDPOINT_URL + '/v1/orgs/search',
                                          data=json.dumps(json_data),
                                          headers=json_headers)

            json_object = json_response.json()
            params["orgs"]= json_object['data']['orgs']
            params["org_type"] = org_type

            params["country_code"] = country_code
            params["state_code"] = state_code
            params["org_name"] = org_name

            return render(request, 'enterlogininfo.html', params)
        except Exception as e:
            logger.error(e)
        return get_countries(request)
