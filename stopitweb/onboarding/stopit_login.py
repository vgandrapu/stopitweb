from django.views.generic import View
from django.conf import settings
from django.shortcuts import render
from base64 import b64encode, b64decode
import requests, json
import logging
from onboarding.stopit_contacts import get_contacts
from onboarding.stopit_getcountries import get_countries, get_org_logininfo

logging.basicConfig()
# Get an instance of a logger
logger = logging.getLogger('project.stopit.logger')


def login(request, intellicode=None):
        params = {}
        logger.error('login got called')
        try:
            if intellicode is None:
                if request.method == 'GET':
                    intellicode = request.session.get('user')
                else:
                    intellicode = request.POST['intellicode']
                    for key, value in request.POST.iteritems():
                        logger.error(key)
                        logger.error(value)
                    if 'accept' not in request.POST:
                        return render(request, 'options.html', params)

            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_data = {'intellicode':intellicode, 'serial_number':'website'}
            json_response = requests.post(settings.API_ENDPOINT_URL + '/v1/devices/register', data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()
            params["name"] = "success"
            logging.debug('Response Code: ' + str(json_response.status_code))
            json_object = json_response.json()
            if json_response.status_code == 200:
                if json_object.get('code')=='100000':
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['intellicode']:
                       request.session['user'] = json_object['data']['user']['intellicode']
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['salt']:
                       request.session['salt'] = json_object['data']['user']['salt']
                    if json_object['data'] and json_object['data']['org'] and json_object['data']['org']['org_id']:
                       request.session['session_org_id'] = json_object['data']['org']['org_id']
                    if json_object['data'] and json_object['data']['org'] and json_object['data']['org']['user_can_create_contact_in_app']:
                        if str(json_object['data']['org']['user_can_create_contact_in_app']) == str(1):
                            request.session['show_manage_contacts'] = 1

                    get_contacts(request)
                    params["contacts"] = request.session['contacts']
                    return render(request, 'incident.html', params)
                else:
                    params["error_message"] = "Invalid Access Code"
                    return render(request, 'accesscode.html', params)
            else:
                params["error_message"] = "Invalid Access Code"
                return render(request, 'accesscode.html', params)
        except:
            params["error_message"] = "Invalid Access Code"
            return render(request, 'accesscode.html', params)


def logout(request):
        # if user is already logged in, sign them out
        request.session.delete()
        params = {}
        return render(request, 'options.html', params)


def loginviainfo(request):
        logger.error('login via info got called')
        # if user is already logged in, sign them out
        request.session.delete()
        try:
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            last_name = request.POST['last_name']
            org_id = request.POST['org_id']
            roster_type = request.POST['roster_type']
            roster_id = request.POST['roster_id']

            if roster_type.lower() != 'email':
                roster_type = "id"


            json_data = {'last_name' : last_name,
                         'serial_number': 'website',
                         'org_id': org_id,
                         'roster_type': roster_type,
                         'roster_id': roster_id}
            logger.error(json_data)

            json_response = requests.post(settings.API_ENDPOINT_URL +'/v1/intellicodes/search', data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()
            params = {}

            params["name"] = "success"
            logger.error(json_object)
            logging.debug('Response Code: ' + str(json_response.status_code))
            json_object = json_response.json()
            if json_response.status_code == 200:
                if json_object.get('code')=='100000':
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['intellicode']:
                       request.session['user'] = json_object['data']['user']['intellicode']
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['salt']:
                       request.session['salt'] = json_object['data']['user']['salt']
                    if json_object['data'] and json_object['data']['org'] and json_object['data']['org']['org_id']:
                       request.session['session_org_id'] = json_object['data']['org']['org_id']
                    return render(request,'incident.html', params)
                else:
                    return render(request,'options.html', params)
            else:
                return render(request,'options.html', params)
        except:
            params["name"] = "Django"
            return render(request,'options.html', params)


def messenger_via_login(request):
        params = {}
        logger.error('login got called')
        try:

            if request.method == 'GET':
                intellicode = request.session.get('user')
            else:
                intellicode = request.POST['intellicode']

            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_data = {'intellicode':intellicode, 'serial_number': 'website'}
            logger.error('intellicode is ' + intellicode)
            json_response = requests.post(settings.API_ENDPOINT_URL+ '/v1/devices/register', data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()
            params["name"] = "success"
            logging.debug('Response Code: ' + str(json_response.status_code))
            json_object = json_response.json()
            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['intellicode']:
                       request.session['user'] = json_object['data']['user']['intellicode']
                       params["ORG_NAME"] = json_object['data']['org']['org_name']
                    return render(request, 'messenger.html', params)
                else:
                    params["error_message"] = "Invalid Access Code"
                    return render(request, 'options.html', params)
            else:
                params["error_message"] = "Invalid Access Code"
                return render(request, 'options.html', params)
        except:
            params["error_message"] = "Invalid Access Code"
            return render(request, 'options.html', params)


def get_toc(request, intellicode = None):
        logger.error('Get TOC got called')
        template_param = {}
        try:

            if intellicode is not None:
                template_param["REQUIRES_ACCEPTANCE"] = 1
                template_param["intellicode"] = intellicode;

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            json_response = requests.get(settings.API_ENDPOINT_URL +'/v1/tos/latest', headers=json_headers)
            json_object = json_response.json()

            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data'] and json_object['data']['system_tos'] and json_object['data']['system_tos']['text']:
                        template_param["tos"] = json_object['data']['system_tos']['text']

            return render(request, 'terms_of_use.html', template_param)
        except:
            return render(request, 'terms_of_use.html', template_param)


def check_toc_acceptance(request):
        logger.error('checking toc acceptance')
        template_param = {}

        try:
            if request.method == 'GET':
                intellicode = request.session.get('user')
            else:
                intellicode = request.POST['intellicode']

            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            json_response = requests.get(settings.API_ENDPOINT_URL +'/v1/intellicodes/' + intellicode + '/checktos', headers=json_headers)
            json_object = json_response.json()

            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data']:
                        accepted_date = json_object['data']['accepted_tos_date']
                        if accepted_date is not None:
                            return login(request, intellicode)
                        else:
                            return get_toc(request, intellicode)
                    else:
                        return get_toc(request, intellicode)
                else:
                    template_param["error_message"] = "Invalid Access Code"
                    return render(request, 'accesscode.html', template_param)
            else:
                template_param["error_message"] = "Invalid Access Code"
                return render(request, 'accesscode.html', template_param)
        except:
            template_param["error_message"] = "Invalid Access Code"
            return render(request, 'accesscode.html', template_param)


def check_tos_for_logininfo(request):
        logger.error('login via info got called')
        # if user is already logged in, sign them out
        request.session.delete()
        try:
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            org_type = request.POST['org_type']
            country_code = request.POST['country_code']
            state_code = request.POST['state_code']
            org_name = request.POST['org_name']

            last_name = request.POST['last_name']
            org_id = request.POST['org_id']
            roster_type = request.POST['roster_type']
            roster_id = request.POST['roster_id']



            if roster_type.lower() != 'email':
                roster_type = "id"

            json_data = {'last_name' : last_name,
                         'serial_number': 'website',
                         'org_id': org_id,
                         'roster_type': roster_type,
                         'roster_id': roster_id}
            logger.error(json_data)

            json_response = requests.post(settings.API_ENDPOINT_URL +'/v1/intellicodes/searchwithtos', data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()

            logger.error(json_object)
            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data']:
                        accepted_date = json_object['data']['accepted_tos_date']
                        intellicode = json_object['data']['intellicode']
                        if accepted_date is not None:
                            request.session['user'] = intellicode
                            return login(request, intellicode)
                        else:
                            return get_toc(request, intellicode)
                    else:
                        return get_org_logininfo(request, org_type=org_type,
                                                 country_code=country_code, state_code=state_code,
                                                 org_name=org_name,
                                                 error_message="Sorry! We couldn't find you. Please try again")
                else:
                    return get_org_logininfo(request, org_type=org_type,
                                                 country_code=country_code, state_code=state_code,
                                                 org_name=org_name,
                                                 error_message="Sorry! We couldn't find you. Please try again")
            else:
                return get_org_logininfo(request, org_type=org_type,
                                                 country_code=country_code, state_code=state_code,
                                                 org_name=org_name,
                                                 error_message="Sorry! We couldn't find you. Please try again")
        except:
            return get_org_logininfo(request, org_type=org_type,
                                                 country_code=country_code, state_code=state_code,
                                                 org_name=org_name,
                                                 error_message="Sorry! We couldn't find you. Please try again")
