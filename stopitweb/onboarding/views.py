from django.views.generic import View
from django.shortcuts import render 
from django.views.generic import TemplateView


class Index(View):
    def get(self, request):
        params = {}
        params["name"] = "Django"
        return render(request, 'options.html', params)


class OptionsView(TemplateView):
    template_name = "options.html"


class accesscode(TemplateView):
    template_name = "accesscode.html"


class findcountry(TemplateView):
    template_name = "findcountry.html"


class incident(TemplateView):
    template_name = "incident.html"


class language(TemplateView):
    template_name = "language.html"

class help(TemplateView):
    template_name = "help.html"
