$(document).ready(function(){
    v=$("#notes");
    org_contact = $("#orgcontact")
    attached_files = $("#attachedfiles")


    $(".anonymous").change(function() {
        if(!this.checked) {
            if (confirm('This report will no longer be submitted anonymously. Are you sure?')) {
            // Save it!
            } else {
                $(".anonymous").prop('checked', true);
            }
        }
    });

    org_contact.bind('click', function(){
        alert("Sorry – your organization has set up this contact to receive all incident reports.");
        return false;
    });

    $('#incidentform').submit(function() {
        var data = $("#incidentform :input").serializeArray();
        if (org_contact.val().length == 0)
        {
            alert("Please select who should receive this report");
            return false;
        }
        if ( ($.trim(v.val()).length == 0) && (attached_files.val() == 0)){
            alert("To send a report, please enter text or attach at least one file.");
            return false;
        }

    return true;
});

});

