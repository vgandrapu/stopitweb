
function widthofpr(p){
 if(p==0){return "0%";}
 return (p/12)*100;
}
function textofpr(p){
     if(p==0)
     {
        return "Please enter your 12 digit access code below.";
     }
     if(p<12)
     {
        return 12 - p + " more";
     }
     if(p>=12)
     {
        return "";
     }
}


$(document).ready(function(){
    v=$("#pass");
    p=$("#pbar");
    pt=$("#ppbartxt");
    pet=$("#ppbarerrortxt");

    v.bind('keydown', function(e){
    if (e.keyCode == 32)
        return false;
    });

    v.bind('keyup',function(){
        p.css('width',widthofpr($.trim(v.val()).length) );
        pt.text(textofpr($.trim(v.val()).length));
        if ($.trim(v.val()).length >= 12)
        {
            $("#next_btn").prop( "disabled", false );
        }else{
             $("#next_btn").prop( "disabled", true );
        }
        if ($.trim(v.val()).length > 0)
        {
            pet.text("");
        }
    });

});

